NAME
  ipaccess-config - Tool to configure an ip.access nanoBTS

SYNOPSIS
  ipaccess-config [options] <IP_OF_BTS>

DESCRIPTION
 ipaccess-config is a small command line tool to configure an ip.access nanoBTS.

 You can use this tool to perform basic operations such as setting NVRAM parameters or performing tests.

 It is part of the OpenBSC base station controller from Osmocom.

OPTIONS

Commands for writing to the BTS:
  -u --unit-id UNIT_ID      Set the Unit ID of the BTS
  -o --oml-ip IP        Set primary OML IP (IP of your BSC)
  -i --ip-address IP/MASK   Set static IP address + netmask of BTS
  -g --ip-gateway IP        Set static IP gateway of BTS
  -r --restart          Restart the BTS (after other operations)
  -n --nvram-flags FLAGS/MASK   Set NVRAM attributes
  -S --nvattr-set FLAG  Set one additional NVRAM attribute
  -U --nvattr-unset FLAG    Set one additional NVRAM attribute
  -l --listen TESTNR        Perform specified test number
  -L --Listen TEST_NAME     Perform specified test
  -s --stream-id ID     Set the IPA Stream Identifier for OML
  -d --software FIRMWARE    Download firmware into BTS

Miscellaneous commands:
  -h --help         this text
  -H --HELP         Print parameter details.
  -f --firmware FIRMWARE    Provide firmware information
  -w --write-firmware       This will dump the firmware parts to the filesystem. Use with -f.
  -p --loop         Loop the tests executed with the --listen command.

SEE ALSO
  ipaccess-find(1)

AUTHOR
  This manual page was written by Ruben Undheim <ruben.undheim@gmail.com> for the Debian project (and may be used by others).



