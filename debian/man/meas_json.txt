NAME
  meas_json - utility to manage measurement reports (osmo-bsc)

SYNOPSIS
  meas_json

DESCRIPTION

  It converts measurement report feed into JSON feed printed to stdout. Each
  measurement report is printed as a separate JSON root entry. All measurement
  reports are separated by a new line.


SEE ALSO
  osmo-bsc(1)

AUTHOR
  This manual page was written by Ruben Undheim <ruben.undheim@gmail.com> for the Debian project (and may be used by others).



