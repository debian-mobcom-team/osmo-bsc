#!/bin/bash


txt2man -d "${CHANGELOG_DATE}" -t OSMO-BSC               -s 1 osmo-bsc.txt             > osmo-bsc.1
txt2man -d "${CHANGELOG_DATE}" -t BS11_CONFIG            -s 1 bs11_config.txt          > bs11_config.1
txt2man -d "${CHANGELOG_DATE}" -t IPACCESS-CONFIG        -s 1 ipaccess-config.txt      > ipaccess-config.1
txt2man -d "${CHANGELOG_DATE}" -t IPACCESS-PROXY         -s 1 ipaccess-proxy.txt       > ipaccess-proxy.1
txt2man -d "${CHANGELOG_DATE}" -t ISDNSYNC               -s 1 isdnsync.txt             > isdnsync.1
txt2man -d "${CHANGELOG_DATE}" -t ABISIP-FIND            -s 1 abisip-find.txt          > abisip-find.1
txt2man -d "${CHANGELOG_DATE}" -t MEAS_JSON              -s 1 meas_json.txt            > meas_json.1
